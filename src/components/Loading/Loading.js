import React from 'react';
import styles from './Loading.module.scss';

const Loading = (props) => {
    return (
        <div className={styles.container}>
            <div className={styles.loading}>
                <svg viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle
                        className="length"
                        fill="none"
                        strokeWidth="8"
                        strokeLinecap="round"
                        cx="33"
                        cy="33"
                        r="28"
                    />
                </svg>
                <svg viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle
                        fill="none"
                        strokeWidth="8"
                        strokeLinecap="round"
                        cx="33"
                        cy="33"
                        r="28"
                    />
                </svg>
                <svg viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle
                        fill="none"
                        strokeWidth="8"
                        strokeLinecap="round"
                        cx="33"
                        cy="33"
                        r="28"
                    />
                </svg>
                <svg viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                    <circle
                        fill="none"
                        strokeWidth="8"
                        strokeLinecap="round"
                        cx="33"
                        cy="33"
                        r="28"
                    />
                </svg>
            </div>
        </div>
    );
};

export default Loading;

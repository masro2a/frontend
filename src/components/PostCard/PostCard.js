import React from 'react';
import { Link } from 'react-router-dom';
import styles from './PostCard.module.scss';
import PropTypes from 'prop-types';

import Card from '../Card/Card';

const PostCard = (props) => {
    return (
        <Card isClickable="true">
            <div className={styles.post}>
                <h2 className={styles.title}>{props.title}</h2>
                <div className={styles.content}>
                    <img
                        className={styles.img}
                        src={props.image}
                        width="150"
                        height="150"
                        alt={props.title}
                    />
                    <div>
                        <span>
                            found by{' '}
                            <Link
                                className={styles.link}
                                to={`/profile/${props.founder.id}`}
                            >
                                {props.founder.name}
                            </Link>{' '}
                            at {new Date(props.foundDate).toLocaleDateString()}
                        </span>

                        <p>{props.description}</p>
                    </div>
                </div>
            </div>
        </Card>
    );
};

PostCard.propTypes = {
    title: PropTypes.string.isRequired,
    founder: PropTypes.object.isRequired,
    foundDate: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};

export default PostCard;

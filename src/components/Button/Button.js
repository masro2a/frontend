import React from 'react';
import styles from './Button.module.scss';

const Button = (props) => {
    return (
        <button
            className={`${props.className} ${styles.btn}`}
            onClick={props.onClick}
        >
            {props.text}
        </button>
    );
};

export default Button;

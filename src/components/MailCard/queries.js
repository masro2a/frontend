import gql from 'graphql-tag';

export default gql`
    mutation decideMail(
        $id: String!
        $isAccepted: Boolean!
    ) {
        acceptEvidence(id: $id) @include(if: $isAccepted)
        
        refuseEvidence(id: $id) @skip(if: $isAccepted)
    }
`;

import React from 'react';
import styles from './MailCard.module.scss';
import PropTypes from 'prop-types';
import { Mutation } from 'react-apollo';
import { Link } from 'react-router-dom';

import Card from '../Card/Card';
import Loading from '../Loading/Loading';
import decideMailQuery from './queries';

const MailCard = (props) => {
    const title =
        props.type === 'informMail' ? (
            <h2>
                <Link to={`/profile/${props.sender.id}`}>
                    {props.sender.name}
                </Link>{' '}
                has reported that{' '}
                <Link to={`/feed/${props.post.id}`}>{props.post.title}</Link>{' '}
                belongs to him
            </h2>
        ) : (
            <h2>
                Your request for{' '}
                <Link to={`/feed/${props.post.id}`}>{props.post.title}</Link>{' '}
                has been
                {props.isAccepted ? ' accepted' : ' refused'}
            </h2>
        );

    return (
        <Mutation mutation={decideMailQuery}>
            {(mutation, { loading, error, data }) => {
                if (loading) {
                    return <Loading />;
                } else if (error) {
                    return <h2>Error</h2>;
                } else {
                    return (
                        <Card isClickable="true">
                            <div>
                                {title}
                                <span>
                                    {new Date(props.date).toLocaleDateString()}
                                </span>
                            </div>
                        </Card>
                    );
                }
            }}
        </Mutation>
    );
};

MailCard.propTypes = {
    post: PropTypes.object.isRequired,
    sender: PropTypes.object.isRequired,
    isAccepted: PropTypes.bool.isRequired,
    date: PropTypes.string.isRequired
};

export default MailCard;

import React, { Component } from "react";
import { Link } from "react-router-dom";

import { FormErrors } from "./FormErrors";
import styles from "./Form.module.scss";
import Button from "../Button/Button";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      password: "",
      formErrors: { name: "", email: "", password: "" },
      nameValid: false,
      emailValid: false,
      passwordValid: false,
      formValid: false
    };
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let nameValid = this.state.nameValid;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch (fieldName) {
      case "email":
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? "" : " is invalid";
        break;
      case "password":
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? "" : " is too short";
        break;
      case "name":
        nameValid = value.length >= 2;
        fieldValidationErrors.name = nameValid ? "" : " is too short";
        break;
      default:
        break;
    }
    this.setState(
      {
        formErrors: fieldValidationErrors,
        nameValid: nameValid,
        emailValid: emailValid,
        passwordValid: passwordValid
      },
      this.validateForm
    );
  }

  validateForm() {
    if (this.props.formType === "signUp") {
      this.setState({
        formValid:
          this.state.nameValid &&
          this.state.emailValid &&
          this.state.passwordValid
      });
    } else {
      this.setState({
        formValid: this.state.emailValid && this.state.passwordValid
      });
    }
  }

  errorClass(error) {
    return error.length === 0 ? "" : "has-error";
  }

  submit = () => {
    const variables = {
      provider: "email",
      email: this.state.email,
      password: this.state.password
    };

    if (this.props.formType === "signUp") {
      variables.name = this.state.name;
    }

    this.props.submit({ variables });
  };

  inputHandler = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value }, () => {
      this.validateField(name, value);
    });
  };

  render() {
    return (
      <div className={styles.container}>
        <h1 className={`logo ${styles.logo}`}>Lost & Found</h1>
        <form className={styles.form}>
          <div className={styles.formContainer}>
            <div className="panel panel-default">
              <FormErrors formErrors={this.state.formErrors} />
            </div>
            {this.props.formType === "signUp" ? (
              <input
                className={`form-group ${this.errorClass(
                  this.state.formErrors.email
                )}`}
                type="text"
                placeholder="Name"
                name="name"
                onChange={this.inputHandler}
                value={this.state.name}
                required
              />
            ) : null}

            <input
              className={`form-group ${this.errorClass(
                this.state.formErrors.email
              )}`}
              type="email"
              placeholder="Email"
              name="email"
              onChange={this.inputHandler}
              value={this.state.email}
              required
            />

            <input
              className={`form-group ${this.errorClass(
                this.state.formErrors.password
              )}`}
              type="password"
              placeholder="Password"
              name="password"
              onChange={this.inputHandler}
              value={this.state.password}
              required
            />
            {this.props.formType === "signUp" ? (
              <Link className={styles.link} to="/signin">
                Already have an account? Sign in.
              </Link>
            ) : (
              <Link className={styles.link} to="/signup">
                Don't have an account? Sign up.
              </Link>
            )}
          </div>
          <Button
            className={styles.btn}
            disabled={!this.state.formValid}
            text={this.props.formType === "signUp" ? "Sign Up" : "Sign In"}
            onClick={this.submit}
          />
        </form>
      </div>
    );
  }
}

export default Form;

import React from "react";
import styles from "./Form.module.scss";

export const FormErrors = ({ formErrors }) => (
  <div className="formErrors">
    {Object.keys(formErrors).map((fieldName, i) => {
      if (formErrors[fieldName].length > 0) {
        return (
          <div className={styles.errorMesseges}>
            <p key={i}>
              {fieldName} {formErrors[fieldName]}
            </p>
          </div>
        );
      } else {
        return "";
      }
    })}
  </div>
);

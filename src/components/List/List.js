import React from 'react';
import styles from './List.module.scss';

const List = (props) => {
    return (
        <div className={styles.container}>
            <div className={styles.fade} />
            <div className={styles.list}>{props.children}</div>
        </div>
    );
};

export default List;

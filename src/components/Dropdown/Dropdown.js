import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Dropdown.module.scss';

const Dropdown = (props) => {
    const listItems = props.map((number) =>
        <li><Link to={props.link}>{props.text}></Link></li>
    );
    return (
        <ul>{listItems}</ul>
    );


};

export default Dropdown;

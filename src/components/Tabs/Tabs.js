import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Tabs.module.scss';
import PropTypes from 'prop-types';

const Tabs = (props) => {
    return (
        <ul className={styles.tabs}>
            {props.tabs.map((tab, i) => (
                <NavLink exact className={styles.tab} key={i} to={tab.route}>
                    {tab.name}
                </NavLink>
            ))}
        </ul>
    );
};

Tabs.propTypes = {
    tabs: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Tabs;

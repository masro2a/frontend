import React from 'react';
import styles from './Page.module.scss';

const Page = (props) => {
    return <div className={styles.page}>{props.children}</div>;
};

export default Page;

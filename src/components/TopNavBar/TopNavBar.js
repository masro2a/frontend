import React, { Component } from 'react';
import { NavLink, Redirect } from 'react-router-dom';

import Client from '../../Helpers/apollo/apolloHandler';

import styles from './TopNavBar.module.scss';

class TopNavBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shouldDirectToRoot: false
        };
    }

    logout = () => {
        Client.writeData({
            data: {
                currentUser: null,
                isLoggedIn: false
            }
        });
        Client.clearStore();
        localStorage.clear();
        this.setState({
            shouldDirectToRoot: true
        });
    };

    render() {
        if (this.state.shouldDirectToRoot) {
            return <Redirect to="/signin" />;
        }

        return (
            <nav className={styles.nav}>
                <div className={styles.tabs}>
                    <h2 className={`logo ${styles.logo}`}>Lost & Found</h2>
                    <NavLink className={styles.tab} to="/feed">
                        Feed
                    </NavLink>
                    <NavLink className={styles.tab} to="/profile">
                        Profile
                    </NavLink>
                    <NavLink className={styles.tab} to="/mail">
                        Mail
                    </NavLink>
                </div>
                <button className={styles.logout} onClick={this.logout}>
                    Log out
                </button>
            </nav>
        );
    }
}

export default TopNavBar;

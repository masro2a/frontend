import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';

import Tabs from '../../components/Tabs/Tabs';
import List from '../../components/List/List';
import MailCard from '../../components/MailCard/MailCard';
import Loading from '../../components/Loading/Loading';

import styles from './Mailbox.module.scss';

import { getMails } from './queries';

export default class Mailbox extends Component {
    constructor(props) {
        super(props);
        this.user = {};
        this.state = {
            mails: [],
            tabs: [
                {
                    name: 'All',
                    route: '/mail'
                }
            ],
            officerTabs: [
                {
                    name: 'All',
                    route: '/mail'
                },
                {
                    name: 'Sent',
                    route: '/mail/sent'
                },
                {
                    name: 'Pending',
                    route: '/mail/pending'
                },
                {
                    name: 'Accepted',
                    route: '/mail/accepted'
                },
                {
                    name: 'Refused',
                    route: '/mail/refused'
                }
            ],
            isOfficer: false,
            filters: {
                '/mail': {},
                '/mail/sent': { filter: 'sent' },
                '/mail/pending': { filter: 'pending' },
                '/mail/accepted': { filter: 'accepted' },
                '/mail/refused': { filter: 'refused' }
            }
        };
    }

    render() {
        return (
            <div>
                <div>
                    <Tabs
                        tabs={
                            this.state.isOfficer
                                ? this.state.officerTabs
                                : this.state.tabs
                        }
                    />
                    <Query
                        query={getMails}
                        variables={this.state.filters[this.props.match.url]}
                        fetchPolicy="network-only"
                    >
                        {({ loading, error, data }) => {
                            if (loading) {
                                return <Loading />;
                            } else if (error) {
                                return <h2>Error: {JSON.stringify(error)}</h2>;
                            } else if (data) {
                                if (
                                    data.me.isOfficer &&
                                    !this.state.isOfficer
                                ) {
                                    this.setState({ isOfficer: true });
                                }
                                return (
                                    <div>
                                        <List>
                                            {data.mailBox.mails.map(
                                                (mail, i) => (
                                                    <Link className={`${styles.link}`} to={`/mail/${mail.id}`}>
                                                        <MailCard
                                                            key={i}
                                                            id={mail.id}
                                                            type={mail.type}
                                                            status={mail.status}
                                                            sender={mail.sender}
                                                            isAccepted={
                                                                mail.isAccepted
                                                            }
                                                            date={mail.date}
                                                            post={mail.post}
                                                            evidence={mail.evidence}
                                                        />
                                                    </Link>
                                                )
                                            )}
                                        </List>
                                    </div>
                                );
                            }
                        }}
                    </Query>
                </div>
            </div>
        );
    }
}

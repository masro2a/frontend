import gql from 'graphql-tag';

export const getMails = gql`
    query mails($filter: MailBoxFilter) {
        mailBox(filter: $filter) {
            mails {
                id
                type
                isAccepted
                date
                status
                sender {
                    id
                    name
                }
                post {
                    id
                    title
                }
                evidence {
                    id
                }
            }
        }
        me {
            id
            isOfficer
        }
    }
`;

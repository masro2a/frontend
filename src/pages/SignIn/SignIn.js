import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import Client from '../../Helpers/apollo/apolloHandler';

import styles from './SignIn.module.scss';

import Form from '../../components/Form/Form';
import Loading from '../../components/Loading/Loading';

import { Redirect } from 'react-router-dom';

import SignInQuery from './queries';

export default class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        };
    }

    submit = (mutation) => {
        mutation({
            variables: {
                provider: 'email',
                email: this.state.email,
                password: this.state.password
            }
        });
    };

    storeUserData = (data) => {
        localStorage.setItem('token', data.signinUser.accessToken);
        localStorage.setItem('refreshToken', data.signinUser.refreshToken);
        Client.writeData({
            data: {
                currentUser: data.signinUser.user,
                isLoggedIn: true
            }
        });
    };

    render() {
        return (
            <div>
                <Mutation mutation={SignInQuery}>
                    {(mutation, { loading, error, data }) => {
                        console.log(error);
                        if (loading) {
                            return <Loading />;
                        } else if (data) {
                            this.storeUserData(data);
                            return <Redirect to="/" />;
                        } else {
                            return (
                                <div className={`page-center`}>
                                    <Form formType="signIn" submit={mutation} />
                                </div>
                            );
                        }
                    }}
                </Mutation>
            </div>
        );
    }
}

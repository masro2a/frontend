import gql from 'graphql-tag';

export default gql`
    mutation signinUser(
        $provider: AuthProviders!
        $email: String!
        $password: String
        $id: ID
        $idToken: String
        $accessToken: String
    ) {
        signinUser(
            authProvider: {
                provider: $provider
                email: $email
                password: $password
                id: $id
                idToken: $idToken
                token: $accessToken
            }
        ) {
            user {
                id
                name
                email
                image
                isOfficer
            }
            accessToken
            refreshToken
        }
    }
`;

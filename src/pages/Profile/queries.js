import gql from 'graphql-tag';

export const getUserQuery = gql`
    query getUserData($id: String!) {
        user(id: $id) {
            id
            name
            email
            image
            isOfficer
        }
        currentUser @client {
            id
        }
    }
`;

export const updateUser = gql`
    mutation updateUser(
        $id: String!
        $email: String
        $name: String
        $image: Upload
    ) {
        updateUser(id: $id, email: $email, name: $name, image: $image) {
            id
        }
    }
`;

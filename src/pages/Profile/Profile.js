import React, { Component } from 'react';
import { Query, Mutation } from 'react-apollo';

import { getUserQuery, updateUser } from './queries';

import Button from '../../components/Button/Button';
import Card from '../../components/Card/Card';
import Loading from '../../components/Loading/Loading';

import styles from './Profile.module.scss';

class ProfileForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.getUserData.user.name,
            email: this.props.getUserData.user.email,
            image: ''
        };
    }

    updateUserData = (mutation) => {
        const variables = {
            id: this.props.getUserData.user.id
        };

        if (this.state.name != this.props.getUserData.user.name) {
            variables.name = this.state.name;
        }

        if (this.state.email != this.props.getUserData.user.email) {
            variables.email = this.state.email;
        }

        if (this.state.image) {
            variables.image = this.state.image;
        }

        mutation({ variables });
    };

    render() {
        const { getUserLoading, getUserError, getUserData } = this.props;

        return (
            <Mutation mutation={updateUser}>
                {(mutation, { loading, error }) => {
                    if (getUserLoading || loading) {
                        return <Loading />;
                    } else if (error || getUserError) {
                        return <h2>Error</h2>;
                    } else {
                        return (
                            <Card>
                                <div className={styles.profilePic}>
                                    <label htmlFor="profile-pic">
                                        {getUserData.currentUser.id ===
                                            getUserData.user.id && (
                                            <div className={styles.uploadHover}>
                                                +
                                            </div>
                                        )}
                                        <img
                                            src={getUserData.user.image}
                                            height="250"
                                            width="250"
                                            alt={getUserData.user.name}
                                        />
                                        <input
                                            name="image"
                                            type="file"
                                            id="profile-pic"
                                            onChange={(e) =>
                                                this.setState({
                                                    image: e.target.files[0]
                                                })
                                            }
                                        />
                                    </label>
                                </div>
                                <div className={styles.container}>
                                    <label className={styles.inputContainer}>
                                        Name:
                                        <input
                                            name="name"
                                            placeholder="name"
                                            type="text"
                                            disabled={
                                                getUserData.currentUser.id !==
                                                getUserData.user.id
                                            }
                                            onChange={(e) =>
                                                this.setState({
                                                    name: e.target.value
                                                })
                                            }
                                            value={this.state.name}
                                        />
                                    </label>
                                    <label className={styles.inputContainer}>
                                        Email:
                                        <input
                                            name="email"
                                            placeholder="email"
                                            type="email"
                                            disabled={
                                                getUserData.currentUser.id !==
                                                getUserData.user.id
                                            }
                                            onChange={(e) =>
                                                this.setState({
                                                    email: e.target.value
                                                })
                                            }
                                            value={this.state.email}
                                        />
                                    </label>
                                    {getUserData.currentUser.id ===
                                        getUserData.user.id && (
                                        <Button
                                            text="Update"
                                            className={styles.btn}
                                            onClick={() =>
                                                this.updateUserData(mutation)
                                            }
                                        />
                                    )}
                                </div>
                            </Card>
                        );
                    }
                }}
            </Mutation>
        );
    }
}

export default (props) => (
    <Query query={getUserQuery} variables={{ id: props.match.params.id }}>
        {({ loading, error, data }) => {
            if (loading) {
                return <Loading />;
            } else if (data) {
                return (
                    <ProfileForm
                        getUserLoading={loading}
                        getUserError={error}
                        getUserData={data}
                    />
                );
            }
        }}
    </Query>
);

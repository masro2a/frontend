import React from 'react';

import styles from './Error.module.scss';

const Error = () => {
    return (
        <div className={styles.page}>
            <h1 className={styles.title}>
                apparently this page has been lost, if found please
                <span role="img" aria-label="eyes">
                    ➕
                </span>
                a post{' '}
                <span role="img" aria-label="eyes" className={styles.eyes}>
                    👀
                </span>
            </h1>
        </div>
    );
};

export default Error;

import gql from 'graphql-tag';

export default gql`
    mutation createUser(
        $provider: AuthProviders!
        $name: String!
        $email: String!
        $password: String
        $id: ID
        $idToken: String
        $accessToken: String
    ) {
        createUser(
            authProvider: {
                provider: $provider
                name: $name
                email: $email
                password: $password
                id: $id
                idToken: $idToken
                token: $accessToken
            }
        ) {
            id
        }
        signinUser(
            authProvider: {
                provider: $provider
                email: $email
                password: $password
                id: $id
                idToken: $idToken
                token: $accessToken
            }
        ) {
            user {
                id
                name
                email
                image
                isOfficer
            }
            accessToken
            refreshToken
        }
    }
`;

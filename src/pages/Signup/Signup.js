import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import Client from '../../Helpers/apollo/apolloHandler';

import Form from '../../components/Form/Form';
import Loading from '../../components/Loading/Loading';

import { Link, Redirect } from 'react-router-dom';

import signUpQuery from './queries';

export default class Signup extends Component {
    submit = (mutation) => {
        mutation({
            variables: {
                provider: 'email',
                name: this.state.name,
                email: this.state.email,
                password: this.state.password
            }
        });
    };

    storeUserData = (data) => {
        localStorage.setItem('token', data.signinUser.accessToken);
        localStorage.setItem('refreshToken', data.signinUser.refreshToken);
        Client.writeData({
            data: {
                currentUser: data.signinUser.user,
                isLoggedIn: true
            }
        });
    };

    render() {
        return (
            <div>
                <Mutation mutation={signUpQuery}>
                    {(mutation, { loading, error, data }) => {
                        if (loading) {
                            return <Loading />;
                        } else if (error) {
                            return <h2>Error: {JSON.stringify(error)}</h2>;
                        } else if (data) {
                            this.storeUserData(data);
                            return <Redirect to="/" />;
                        } else {
                            return (
                                <div className={`page-center`}>
                                    <Form formType="signUp" submit={mutation} />
                                </div>
                            );
                        }
                    }}
                </Mutation>
            </div>
        );
    }
}

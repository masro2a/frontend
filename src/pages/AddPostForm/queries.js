import gql from 'graphql-tag';

export default gql`
    mutation createPost(
        $title: String!
        $description: String!
        $foundDate: Date!
        $image: Upload!
    ) {
        createPost(
            title: $title
            description: $description
            foundDate: $foundDate
            image: $image
        ) {
            id
        }
    }
`;

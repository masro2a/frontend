import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import Button from '../../components/Button/Button';
import Card from '../../components/Card/Card';
import Loading from '../../components/Loading/Loading';

import styles from './AddPostForm.module.scss';

import addPostQuery from './queries';

class AddPostForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            image: '',
            previewImage: '',
            title: '',
            foundDate: new Date(),
            description: ''
        };
    }

    submit = (mutation) => {
        mutation({
            variables: {
                image: this.state.image,
                title: this.state.title,
                foundDate: this.state.foundDate.toISOString(),
                description: this.state.description
            }
        });
    };

    uploadImage = (event) => {
        this.setState({
            image: event.target.files[0]
        });
        const that = this;
        var reader = new FileReader();
        reader.onload = function(e) {
            that.setState({
                previewImage: e.target.result
            });
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    render() {
        return (
            <Mutation mutation={addPostQuery}>
                {(mutation, { loading, error, data }) => {
                    if (loading) {
                        return <Loading />;
                    } else if (error) {
                        return <h2>Error: {JSON.stringify(error)}</h2>;
                    } else if (data) {
                        console.log(data);
                        return <Redirect to="/feed" />;
                    } else {
                        return (
                            <Card>
                                <div className={styles.postImg}>
                                    <label htmlFor="post-img">
                                        {!this.state.previewImage && (
                                            <div className={styles.imgEmpty}>
                                                Upload image
                                            </div>
                                        )}
                                        <div className={styles.uploadHover}>
                                            +
                                        </div>
                                        <img
                                            src={this.state.previewImage}
                                            height="250"
                                            width="250"
                                            alt=""
                                        />
                                        <input
                                            name="image"
                                            type="file"
                                            id="post-img"
                                            onChange={this.uploadImage}
                                        />
                                    </label>
                                </div>
                                <div>
                                    <div className={styles.container}>
                                        <label
                                            className={styles.inputContainer}
                                        >
                                            Title:
                                            <input
                                                type="text"
                                                name="title"
                                                onChange={(e) =>
                                                    this.setState({
                                                        title: e.target.value
                                                    })
                                                }
                                                value={this.state.title}
                                            />
                                        </label>
                                        <label
                                            className={styles.inputContainer}
                                        >
                                            Found Date:
                                            <input
                                                type="date"
                                                name="found-date"
                                                onChange={(e) =>
                                                    this.setState({
                                                        foundDate: new Date(
                                                            e.target.value
                                                        )
                                                    })
                                                }
                                                value={this.state.foundDate
                                                    .toISOString()
                                                    .substr(0, 10)}
                                            />
                                        </label>
                                    </div>
                                    <label>
                                        <textarea
                                            name="description"
                                            placeholder="description"
                                            className={styles.description}
                                            onChange={(e) =>
                                                this.setState({
                                                    description: e.target.value
                                                })
                                            }
                                        />
                                    </label>
                                </div>
                                <Button
                                    className={styles.btn}
                                    text="+ Add Post"
                                    onClick={() => this.submit(mutation)}
                                />
                            </Card>
                        );
                    }
                }}
            </Mutation>
        );
    }
}

export default AddPostForm;

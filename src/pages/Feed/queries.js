import gql from 'graphql-tag';

export const getAllPosts = gql`
    {
        feed {
            posts {
                id
                title
                description
                foundDate
                image
                founder {
                    id
                    name
                    image
                }
            }
        }
    }
`;

export const getMyPosts = gql`
    {
        me {
            id
            createdPosts {
                id
                title
                description
                foundDate
                image
                founder {
                    id
                    name
                    image
                }
            }
        }
    }
`;

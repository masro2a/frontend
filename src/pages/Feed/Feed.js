import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo';

import Tabs from '../../components/Tabs/Tabs';
import List from '../../components/List/List';
import Loading from '../../components/Loading/Loading';
import PostCard from '../../components/PostCard/PostCard';

import styles from './Feed.module.scss';

import { getAllPosts, getMyPosts } from './queries';

export default class Feed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tabs: [
                {
                    name: 'All',
                    route: '/feed'
                },
                {
                    name: 'My',
                    route: '/feed/my'
                }
            ]
        };
    }

    render() {
        return (
            <div>
                <div className={styles.nav}>
                    <Tabs tabs={this.state.tabs} />
                    <Link className={styles.addBtn} to="/feed/add">
                        Add post
                    </Link>
                </div>
                <Query
                    query={
                        this.props.match.url !== '/feed/my'
                            ? getAllPosts
                            : getMyPosts
                    }
                    fetchPolicy="network-only"
                >
                    {({ loading, error, data }) => {
                        if (loading) {
                            return <Loading />;
                        } else if (error) {
                            return <h2>Error: {JSON.stringify(error)}</h2>;
                        } else if (data) {
                            const posts =
                                this.props.match.url !== '/feed/my'
                                    ? data.feed.posts
                                    : data.me.createdPosts;
                            return (
                                <div>
                                    <List>
                                        {posts.map((post, i) => (
                                            <Link
                                                key={i}
                                                className={styles.link}
                                                to={`/feed/${post.id}`}
                                            >
                                                <PostCard
                                                    title={post.title}
                                                    founder={post.founder}
                                                    foundDate={post.foundDate}
                                                    image={post.image}
                                                    description={
                                                        post.description
                                                    }
                                                />
                                            </Link>
                                        ))}
                                    </List>
                                </div>
                            );
                        }
                    }}
                </Query>
            </div>
        );
    }
}

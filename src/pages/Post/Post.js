import React, { Component } from 'react';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import styles from './Post.module.scss';
import PropTypes from 'prop-types';

import { getPostQuery } from './queries';

import Button from '../../components/Button/Button';
import Loading from '../../components/Loading/Loading';

import Card from '../../components/Card/Card';

class Post extends Component {
    render() {
        return (
            <Query
                query={getPostQuery}
                variables={{ id: this.props.match.params.id }}
            >
                {({ loading, error, data }) => {
                    if (loading) {
                        return <Loading />;
                    } else if (error) {
                        return <h2>error {JSON.stringify(error)}</h2>;
                    } else if (data) {
                        return (
                            <Card>
                                <div className={styles.header}>
                                    <h1>{data.post.title}</h1>
                                    <span className={styles.foundBy}>
                                        found by{' '}
                                        <Link
                                            className={styles.link}
                                            to={`/profile/${
                                                data.post.founder.id
                                            }`}
                                        >
                                            {data.post.founder.name}
                                        </Link>{' '}
                                        at{' '}
                                        <span>
                                            {new Date(
                                                data.post.foundDate
                                            ).toLocaleDateString()}
                                        </span>
                                    </span>
                                </div>
                                <div>
                                    <img
                                        className={styles.image}
                                        src={data.post.image}
                                        height="300"
                                        alt={data.post.title}
                                    />
                                    <p className={styles.description}>
                                        {data.post.description}
                                    </p>
                                    {data.me.id !== data.post.founder.id && (
                                        <div className={styles.request}>
                                            <h3>
                                                Does this item belong to you?
                                            </h3>
                                            <Link
                                                to={`/feed/${
                                                    this.props.match.params.id
                                                }/evidence`}
                                                className={styles.btn}
                                            >
                                                Send Evidence
                                            </Link>
                                        </div>
                                    )}
                                </div>
                            </Card>
                        );
                    }
                }}
            </Query>
        );
    }
}

Post.propTypes = {
    title: PropTypes.string.isRequired,
    founder: PropTypes.object.isRequired,
    foundDate: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
};

export default Post;

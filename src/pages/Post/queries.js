import gql from 'graphql-tag';

export const getPostQuery = gql`
    query getPostData($id: String!) {
        post(id: $id) {
            id
            title
            description
            foundDate
            image
            founder {
                id
                name
            }
        }
        me {
            id
        }
    }
`;

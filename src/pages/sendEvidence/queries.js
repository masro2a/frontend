import gql from 'graphql-tag';

export default gql`
    mutation sendEvidence(
        $postId: String!
        $message: String!
        $images: [Upload!]!
    ) {
        sendEvidence(postId: $postId, message: $message, images: $images)
    }
`;

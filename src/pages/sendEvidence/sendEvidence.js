import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import { Redirect } from 'react-router-dom';

import Button from '../../components/Button/Button';
import Card from '../../components/Card/Card';
import Loading from '../../components/Loading/Loading';

import styles from './sendEvidence.module.scss';

import sendEvidenceMutation from './queries';

export default class SendEvidence extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: '',
            message: ''
        };
    }

    submit = (mutation) => {
        mutation({
            variables: {
                images: this.state.images,
                message: this.state.message,
                postId: this.props.match.params.id
            }
        });
    };

    uploadImage = (event) => {
        this.setState({
            images: event.target.files
        });
        const that = this;
        var reader = new FileReader();
        reader.onload = function(e) {
            that.setState({
                previewImage: e.target.result
            });
        };
        reader.readAsDataURL(event.target.files[0]);
    };

    render() {
        return (
            <Mutation mutation={sendEvidenceMutation}>
                {(mutation, { loading, error, data }) => {
                    if (loading) {
                        return <Loading />;
                    } else if (error) {
                        return <h2>Error: {JSON.stringify(error)}</h2>;
                    } else if (data) {
                        return <Redirect to="/feed" />;
                    } else {
                        return (
                            <Card>
                                <div className={styles.evidenceImg}>
                                    <label htmlFor="post-img">
                                        {!this.state.previewImage && (
                                            <div className={styles.imgEmpty}>
                                                Upload image
                                            </div>
                                        )}
                                        <div className={styles.uploadHover}>
                                            +
                                        </div>
                                        <img
                                            src={this.state.previewImage}
                                            height="250"
                                            width="250"
                                            alt=""
                                        />
                                        <input
                                            name="image"
                                            type="file"
                                            id="post-img"
                                            multiple
                                            onChange={this.uploadImage}
                                        />
                                    </label>
                                </div>
                                <textarea
                                    className={styles.message}
                                    name="message"
                                    placeholder="message"
                                    onChange={(e) =>
                                        this.setState({
                                            message: e.target.value
                                        })
                                    }
                                    value={this.state.title}
                                />
                                <Button
                                    className={styles.btn}
                                    text="Send Evidence"
                                    onClick={() => this.submit(mutation)}
                                />
                            </Card>
                        );
                    }
                }}
            </Mutation>
        );
    }
}

import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

import { Mutation, Query } from 'react-apollo';

import Card from '../../components/Card/Card';
import Loading from '../../components/Loading/Loading';

import { decideMailMutation, getMailQuery } from './queries';

import styles from './Mail.module.scss';

class Mail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToMail: false
        };
    }

    onButtonClick = (mutation, id, isAccepted) => {
        mutation({
            variables: {
                id,
                isAccepted
            }
        });
        this.setState({ redirectToMail: true });
    };

    render() {
        const { getMailLoading, getMailError, getMailData } = this.props;

        return (
            <Mutation mutation={decideMailMutation}>
                {(mutation, { loading, error }) => {
                    if (getMailLoading || loading) {
                        return <Loading />;
                    } else if (error || getMailError) {
                        return <h2>Error {JSON.stringify(error)}</h2>;
                    } else {
                        if (this.state.redirectToMail) {
                            return <Redirect to="/mail" />;
                        }
                        const title =
                            getMailData.mail.type === 'informMail' ? (
                                <h2>
                                    <Link
                                        to={`/profile/${
                                            getMailData.mail.sender.id
                                        }`}
                                    >
                                        {getMailData.mail.sender.name}
                                    </Link>{' '}
                                    has reported that{' '}
                                    <Link
                                        to={`/feed/${getMailData.mail.post.id}`}
                                    >
                                        {getMailData.mail.post.title}
                                    </Link>{' '}
                                    belongs to him
                                </h2>
                            ) : (
                                <h2>
                                    Your request for{' '}
                                    <Link
                                        to={`/feed/${getMailData.mail.post.id}`}
                                    >
                                        {getMailData.mail.post.title}
                                    </Link>{' '}
                                    has been
                                    {getMailData.mail.isAccepted
                                        ? ' accepted'
                                        : ' refused'}
                                </h2>
                            );
                        return (
                            <Card>
                                <div className={styles.header}>
                                    {title}
                                    <span>
                                        {new Date(
                                            getMailData.mail.date
                                        ).toLocaleDateString()}
                                    </span>
                                </div>
                                <div className={styles.post}>
                                    <h2 className={styles.postTitle}>Post</h2>
                                    <div className={styles.postContent}>
                                        <img
                                            className={styles.postImg}
                                            src={getMailData.mail.post.image}
                                            width="150"
                                            height="150"
                                            alt={getMailData.mail.post.title}
                                        />
                                        <div>
                                            <span>
                                                found by{' '}
                                                <Link
                                                    className={styles.link}
                                                    to={`/profile/${
                                                        getMailData.mail.post
                                                            .founder.id
                                                    }`}
                                                >
                                                    {
                                                        getMailData.mail.post
                                                            .founder.name
                                                    }
                                                </Link>{' '}
                                                at{' '}
                                                {new Date(
                                                    getMailData.mail.post.foundDate
                                                ).toLocaleDateString()}
                                            </span>

                                            <p>
                                                {
                                                    getMailData.mail.post
                                                        .description
                                                }
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.evidence}>
                                    <h2>Evidence</h2>
                                    <div className={styles.evidenceImgs}>
                                        {getMailData.mail.evidence.images.map(
                                            (image) => (
                                                <img
                                                    src={image}
                                                    width="150"
                                                    height="150"
                                                    alt=""
                                                />
                                            )
                                        )}
                                    </div>
                                    <p>{getMailData.mail.evidence.message}</p>
                                </div>
                                {getMailData.currentUser.isOfficer &&
                                    getMailData.mail.type === 'informMail' &&
                                    getMailData.mail.status === 'pending' && (
                                        <div className={styles.btnsContainer}>
                                            <button
                                                className={styles.btn}
                                                onClick={() =>
                                                    this.onButtonClick(
                                                        mutation,
                                                        getMailData.mail
                                                            .evidence.id,
                                                        true
                                                    )
                                                }
                                            >
                                                Accept
                                            </button>
                                            <button
                                                className={`${styles.btn} ${
                                                    styles.btnRed
                                                }`}
                                                onClick={() =>
                                                    this.onButtonClick(
                                                        mutation,
                                                        getMailData.mail
                                                            .evidence.id,
                                                        false
                                                    )
                                                }
                                            >
                                                Refuse
                                            </button>
                                        </div>
                                    )}
                            </Card>
                        );
                    }
                }}
            </Mutation>
        );
    }
}

export default (props) => (
    <Query query={getMailQuery} variables={{ id: props.match.params.id }}>
        {({ loading, error, data }) => {
            if (loading) {
                return <Loading />;
            } else if (error) {
                return <h2>error {JSON.stringify(error)}</h2>;
            } else if (data) {
                return (
                    <Mail
                        getMailLoading={loading}
                        getMailError={error}
                        getMailData={data}
                    />
                );
            }
        }}
    </Query>
);

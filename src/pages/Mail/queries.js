import gql from 'graphql-tag';

export const decideMailMutation = gql`
    mutation decideMail($id: String!, $isAccepted: Boolean!) {
        acceptEvidence(id: $id) @include(if: $isAccepted)

        refuseEvidence(id: $id) @skip(if: $isAccepted)
    }
`;

export const getMailQuery = gql`
    query getMailData($id: String!) {
        mail(id: $id) {
            type
            status
            isAccepted
            date
            sender {
                id
                name
            }
            post {
                id
                title
                description
                image
                foundDate
                founder {
                    id
                    name
                }
            }
            evidence {
                id
                message
                images
            }
        }
        currentUser @client {
            id
            isOfficer
        }
    }
`;

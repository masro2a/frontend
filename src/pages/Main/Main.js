import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import TopNavBar from '../../components/TopNavBar/TopNavBar';

import FeedRouter from '../../routers/FeedRouter/FeedRouter';
import MailRouter from '../../routers/MailRouter/MailRouter';
import ProfileRouter from '../../routers/ProfileRouter/ProfileRouter';
import Error from '../Error/Error';
import Page from '../../components/Page/Page';

export default class Home extends Component {
    render() {
        return (
            <Page>
                <TopNavBar />
                <Switch>
                    <Route path="/" exact component={FeedRouter} />
                    <Route path="/feed" component={FeedRouter} />
                    <Route path="/mail" component={MailRouter} />
                    <Route path="/Profile" component={ProfileRouter} />
                    <Route component={Error} />
                </Switch>
            </Page>
        );
    }
}

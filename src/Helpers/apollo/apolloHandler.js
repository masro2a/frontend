import { backendUrl } from '../const';
import { resolvers, typeDefs } from './resolvers';
import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { persistCache } from 'apollo-cache-persist';
import { createUploadLink } from 'apollo-upload-client';
import { setContext } from "apollo-link-context";

const cache = new InMemoryCache();

export const hydrateCache = async () => {
    await persistCache({
        cache,
        storage: localStorage,
        debug: true
    });
};


const authMiddleware = setContext(async (req, previousContext) => {
    // if you have a cached value, return it immediately
    
    const accessToken = localStorage.getItem('token');
  
    if (!accessToken) {
      return;
    }
  
    return {
      headers: {
        ...previousContext.headers,
        authorization: `bearer ${accessToken}`
      }
    }
});

export default new ApolloClient({
    link: ApolloLink.from([
        authMiddleware,
        createUploadLink({
            uri: backendUrl,
            headers: {
                authorization: `bearer ${localStorage.getItem('token')}`
            }
        })
    ]),
    cache,
    initializers: {
        isLoggedIn: () => false,
        currentUser: () => null
    },
    resolvers,
    typeDefs
});

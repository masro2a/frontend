import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Mail from '../../pages/Mail/Mail';
import Mailbox from '../../pages/Mailbox/Mailbox';

export default class MailRouter extends Component {
    render() {
        return (
            <Switch>
                <Route path="/mail" exact component={Mailbox} />
                <Route path="/mail/sent" exact component={Mailbox} />
                <Route path="/mail/pending" exact component={Mailbox} />
                <Route path="/mail/accepted" exact component={Mailbox} />
                <Route path="/mail/refused" exact component={Mailbox} />
                <Route path="/mail/:id" exact component={Mail} />
            </Switch>
        );
    }
}

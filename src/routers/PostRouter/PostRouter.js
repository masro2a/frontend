import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Post from '../../pages/Post/Post';
import SendEvidence from '../../pages/sendEvidence/sendEvidence';

export default class PostRouter extends Component {
    render() {
        return (
            <Switch>
                <Route
                    path="/"
                    exact
                    component={() => <Redirect to="/feed" />}
                />
                <Route path="/feed/:id" exact component={Post} />
                <Route
                    path="/feed/:id/evidence"
                    exact
                    component={SendEvidence}
                />
            </Switch>
        );
    }
}

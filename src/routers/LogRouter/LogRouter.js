import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';

import SignIn from '../../pages/SignIn/SignIn';
import Signup from '../../pages/Signup/Signup';

export default class LogRouter extends Component {
    render() {
        return (
            <Switch>
                <Route path="/signin" exact component={SignIn} />
                <Route path="/signup" exact component={Signup} />
                <Route component={() => <Redirect to="/signin" />} />
            </Switch>
        );
    }
}

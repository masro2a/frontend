import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Feed from '../../pages/Feed/Feed';
import PostRouter from '../PostRouter/PostRouter';
import AddPostForm from '../../pages/AddPostForm/AddPostForm';

export default class FeedRouter extends Component {
    render() {
        return (
            <Switch>
                <Route
                    path="/"
                    exact
                    component={() => <Redirect to="/feed" />}
                />
                <Route path="/feed" exact component={Feed} />
                <Route path="/feed/my" exact component={Feed} />
                <Route path="/feed/add" exact component={AddPostForm} />
                <Route path="/feed/:id" component={PostRouter} />
            </Switch>
        );
    }
}

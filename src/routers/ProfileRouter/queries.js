import gql from 'graphql-tag';

export const getUserId = gql`
    {
        me {
            id
        }
    }
`;

import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { Query } from 'react-apollo';

import { getUserId } from './queries';

import Profile from '../../pages/Profile/Profile';
import Loading from '../../components/Loading/Loading';

export default class ProfileRouter extends Component {
    render() {
        return (
            <Switch>
                <Route
                    path="/Profile"
                    exact
                    component={() => (
                        <Query query={getUserId}>
                            {({ loading, error, data }) => {
                                if (loading) {
                                    return <Loading />;
                                } else if (data) {
                                    console.log(data);
                                    return (
                                        <Redirect
                                            to={`/profile/${data.me.id}`}
                                        />
                                    );
                                }
                            }}
                        </Query>
                    )}
                />
                <Route path="/profile/:id" exact component={Profile} />
            </Switch>
        );
    }
}

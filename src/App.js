import React, { Component } from 'react';
import './App.scss';

import LogRouter from './routers/LogRouter/LogRouter';
import Main from './pages/Main/Main';

import gql from 'graphql-tag';

import client, { hydrateCache } from './Helpers/apollo/apolloHandler';

const isLoggedInQuery = gql`
    query isLoggedIn {
        isLoggedIn
    }
`;

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        };
    }

    componentDidMount() {
        hydrateCache().then(() => this.setState({ loading: false }));
    }

    render() {
        if (this.state.loading) {
            return <h1>Loading</h1>;
        } else {
            const { isLoggedIn } = client.readQuery({ query: isLoggedInQuery });

            return isLoggedIn ? <Main /> : <LogRouter />;
        }
    }
}

export default App;
